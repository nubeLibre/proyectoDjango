#django2021

import datetime

from django.db import models
from django.utils import timezone
from django.contrib import admin


class Question(models.Model):
##    categoria = models.ForeignKey(Categoria,  on_delete=models.CASCADE)
    question_text = models.CharField("Pregunta",max_length=200)
    pub_date = models.DateTimeField('Fecha de publicación')
    def __str__(self):
        return self.question_text

    ## vista de question en admin    
    @admin.display(
        boolean=True,
        ordering='pub_date',
        description='Publicada recientemente?',
    )

#    def __str__(self):
#        return self.question_text

        ## devuelve true si la fecha es menor a 1 dia pasado (false si es anterior o futura)
    def was_published_recently(self):
        return timezone.now() >= self.pub_date >= timezone.now() - datetime.timedelta(days=1)

##    def has_choice(self):
##        return self.choice__isnull()

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField("Respuesta", max_length=200)
    votes = models.IntegerField("Votos", default=0)

    def __str__(self):
        return self.choice_text
