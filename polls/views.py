
##from django.template import loader
##from django.http import Http404
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.db.models import F
# utilizando F() estamos indicando al ORM de Django que obtenga el valor actual del campo y lo actualice dentro de la misma sentencia SQL.
from django.utils import timezone

from .models import Choice, Question

class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

## se agregó filtro para q no traiga preguntas sin opciones
## y el distinct()
    def get_queryset(self):
        """Retorna las últimas 5 preguntas pubicadas.
        No toma preguntas con fecha futura"""
        return Question.objects.filter(
        pub_date__lte=timezone.now(),
        choice__isnull = False
        ).distinct().order_by('-pub_date')[:5]
        ##  __lte date filter , <= si pub_date es menor o igual a timezone.now.'''

class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'

    def get_queryset(self):
        """
        Excluye preguntas no publicadas a la fecha y
        pregunatas sin opciones
        """
        return Question.objects.filter(pub_date__lte=timezone.now(),
        choice__isnull = False).distinct()

class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'

    def get_queryset(self):
        """
        Excluye preguntas no publicadas a la fecha y
        pregunatas sin opciones
        """
        return Question.objects.filter(pub_date__lte=timezone.now(),
        choice__isnull = False).distinct()

##deberia filtrar las  QUESTION que no tienen choice- 404
def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Vuelve a mostrar form con msg de error
        return render(request, 'polls/detail.html', {
            'question': question,
            'error_message': "Debe elegir una opción",
        })
    else:
        selected_choice.votes = F('votes') + 1
        selected_choice.save()
        # puede ser necesario despues de usar F() :  selected_choice.refresh_from_db()
        # Siempre  return  HttpResponseRedirect despues de guardar POST data.
        # para prevenir duplicacion si presionan Back button.
        return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))
